def exercise_one():
        for i in range(1, 101):
          if i % 3 == 0 and i % 5 == 0:
            print("ThreeFive")
          elif i % 3 == 0:
            print("Three")
          elif i % 5 == 0:
            print("Five")


def find_missing_nb(arr):
    n = len(arr)
    total = (n + 1) * (n + 2) // 2
    SumOfArr = sum(arr)
    return total - SumOfArr

def sort_Array_Without_Changing_Neg(arr):
    # separer les nombres negatifs de la liste
    negative_nums = [x for x in arr if x < 0]
    positive_nums = [x for x in arr if x >= 0]
    # trier les nombres positifs
    positive_nums.sort()
    # concatener les nombres negatifs et les nombres positifs tries
    result = negative_nums + positive_nums
    return result

