

class MySet(set):
    # todo exercise 1
     def __init__(self, lst):
        self.lst = list(set(lst))

    def __add__(self, other):
        return MySet(self.lst + other.lst)

    def __sub__(self, other):
        return MySet([x for x in self.lst if x not in other.lst])

#exercice2------------------------------------
def decorator_check_max_int(maxsize):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            if result > maxsize:
                raise ValueError(f"Result exceeds maximum value of {maxsize}")
            return result
        return wrapper
    return _decorator

@decorator_check_max_int(maxsize=10)
def add(x, y):
    return x + y

#exercice3----------------------------------------
def ignore_exception(exception):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return _decorator

def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)

@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b

@ignore_exception(TypeError)
def raise_something(exception):
    raise exception



# exercise 4-------------------------
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList:
    # todo exercise 5
    pass


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass



class MySet(set):
    # todo exercise 1
     def __init__(self, lst):
        self.lst = list(set(lst))

    def __add__(self, other):
        return MySet(self.lst + other.lst)

    def __sub__(self, other):
        return MySet([x for x in self.lst if x not in other.lst])

#exercice2------------------------------------
def decorator_check_max_int(maxsize):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            if result > maxsize:
                raise ValueError(f"Result exceeds maximum value of {maxsize}")
            return result
        return wrapper
    return _decorator

@decorator_check_max_int(maxsize=10)
def add(x, y):
    return x + y

#exercice3----------------------------------------
def ignore_exception(exception):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return _decorator

def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)

@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b

@ignore_exception(TypeError)
def raise_something(exception):
    raise exception



# exercise 4-------------------------
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList:
    # todo exercise 5
    pass


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

